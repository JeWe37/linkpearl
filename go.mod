module gitlab.com/etke.cc/linkpearl

go 1.18

require (
	github.com/hashicorp/golang-lru/v2 v2.0.1
	github.com/rs/zerolog v1.30.0
	go.mau.fi/util v0.1.0
	maunium.net/go/mautrix v0.16.1
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/tidwall/gjson v1.16.0 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
	github.com/tidwall/sjson v1.2.5 // indirect
	github.com/yuin/goldmark v1.5.6 // indirect
	golang.org/x/crypto v0.13.0 // indirect
	golang.org/x/exp v0.0.0-20230905200255-921286631fa9 // indirect
	golang.org/x/net v0.15.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	maunium.net/go/maulogger/v2 v2.4.1 // indirect
)
